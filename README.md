GitLab Pages for Beverages Website.

Visit https://dexochs.gitlab.io/beverages

PDFs are loaded from Google Drive.

Character set for font optimising:
- andale-mono.woff2: ` "BGKTaefhiknrtwy`
- courier.woff2: ` ∙&'()-=ABCDEFGHKLMNOPRSTVWabcdefghijklmnopqrstuwyzú`
